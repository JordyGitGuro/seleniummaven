import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class SeleniumTest {
    @Test
    public void InitTest() {
        WebDriverManager.chromedriver()
                .proxy("10.0.0.10:80")
                .browserVersion("86")
                .setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.youtube.com/");
    }
}